# VARTISTE

![logo](./src/assets/vartiste.png)

VARTISTE is a 2D / Surface drawing and painting app for virtual reality.

![moose](./src/static/moose.png)

- [Launch VARTISTE](https://zach-geek.gitlab.io/vartiste/index.html)
- [Instructions](https://zach-geek.gitlab.io/vartiste/landing.html)
- [Source Code](https://gitlab.com/zach-geek/vartiste)
